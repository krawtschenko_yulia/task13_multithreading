import java.io.IOException;
import java.io.PipedWriter;

public class PipedWriterTask implements Runnable {
    private String name;
    private PipedWriter writer;

    public PipedWriterTask(String name, PipedWriter writer) {
        this.name = name;
        this.writer = writer;
    }

    @Override
    public void run() {
        for (int i = 0; i < 11; i++) {
            try {
                writer.write(i);
                writer.flush();

                Thread.sleep(1000);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
