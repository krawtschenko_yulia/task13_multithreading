import java.util.Random;

public class Sleeper implements Runnable {
    private static Random random = new Random();
    private int min;
    private int max;

    public Sleeper(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public void run() {
        int sleepTime = random.nextInt((max - min) + 1) + min;

        try {
            Thread.sleep(sleepTime * 1000);

            System.out.println(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
