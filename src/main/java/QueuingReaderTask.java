import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

public class QueuingReaderTask implements Callable<Integer> {
    private String name;
    private BlockingQueue<Integer> queue;

    public QueuingReaderTask(String name, BlockingQueue<Integer> queue) {
        this.name = name;
        this.queue = queue;
    }

    @Override
    public Integer call() throws Exception {
        return queue.take();
    }
}
