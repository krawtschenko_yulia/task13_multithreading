import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

public class QueuingWriterTask implements Callable {
    private String name;
    private BlockingQueue<Integer> queue;

    public QueuingWriterTask(String name, BlockingQueue<Integer> queue) {
        this.name = name;
        this.queue = queue;
    }

    @Override
    public Integer call() throws Exception {
        for (int i = 0; i < 11; i++) {
            queue.put(i);

            Thread.sleep(1000);
        }
        return 0;
    }
}
