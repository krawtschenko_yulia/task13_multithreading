public interface PingPongInterface {
    PingPongRunnable ping = null;
    PingPongRunnable pong = null;

    void throwBall() throws InterruptedException;
}
