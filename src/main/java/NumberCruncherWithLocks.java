import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

public class NumberCruncherWithLocks {
    final List<Integer> numbers;
    final Object even = new Object();
    final Object odd = new Object();
    final Object relativelyPrime = new Object();
    final ReadWriteLock lock = new ReentrantReadWriteLock();

    public NumberCruncherWithLocks() {
        numbers = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    }

    public List<Integer> removeEvenNumbers() {
        lock.writeLock().lock();

        try {
            numbers.removeIf(this::isEven);

            return numbers;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public List<Integer> removeOddNumbers() {
        lock.writeLock().lock();

        try {
            numbers.removeIf(this::isOdd);

            return numbers;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public List<Integer> removeRelativelyPrimeNumbers() {
        lock.writeLock().lock();

        try {
            numbers.removeIf(this::isRelativelyPrime);

            return numbers;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public List<Integer> getEvenNumbers() {
        lock.readLock().lock();

        List<Integer> evenNumbers;
        try {
            evenNumbers = numbers.stream()
                    .filter(this::isEven)
                    .collect(Collectors.toList());
        } finally {
            lock.readLock().unlock();
        }
        return evenNumbers;
    }

    public List<Integer> getOddNumbers() {
        lock.readLock().lock();

        List<Integer> oddNumbers;
        try {
            oddNumbers = numbers.stream()
                    .filter(this::isOdd)
                    .collect(Collectors.toList());
        } finally {
            lock.readLock().unlock();
        }
        return oddNumbers;
    }

    public List<Integer> getRelativelyPrimeNumbers() {
        lock.readLock().lock();

        List<Integer> relativelyPrimeNumbers;
        try {
            relativelyPrimeNumbers = numbers.stream()
                    .filter(this::isRelativelyPrime)
                    .collect(Collectors.toList());
        } finally {
            lock.readLock().unlock();
        }
        return relativelyPrimeNumbers;
    }

    private boolean isEven(int number) {
        return number % 2 == 0;
    }

    private boolean isOdd(int number) {
        return number % 2 != 0;
    }

    private boolean isRelativelyPrime(int number) {
        lock.readLock().lock();;

        try {
            for (Integer n : numbers) {
                if (number != n && n % number == 0) {
                    return false;
                }
            }
        } finally {
            lock.readLock().unlock();
        }
        return true;
    }
}
