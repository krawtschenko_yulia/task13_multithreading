import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FibonacciGenerator {
    private final List<Integer> list = new ArrayList<>();

    public FibonacciGenerator() {
        list.add(0);
        list.add(1);
    }

    public List<Integer> generateList(final int size)
            throws InterruptedException, ExecutionException {
        return generate(size, synced);
    }

    public int getSum() throws Exception {
        FibonacciSummer summer = new FibonacciSummer(list);

        return summer.call();
    }

    private Runnable calculateNextElement = new Runnable() {
        @Override
        public void run() {
            synchronized (list) {
                try {
                    wait();

                    int last = list.get(lastIndex());
                    int oneBefore = list.get(oneBeforeLastIndex());
                    list.add(last + oneBefore);

                    notifyAll();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private Callable<Integer> calculateNext = new Callable<Integer>() {
        @Override
        public Integer call() throws Exception {
            int last = list.get(lastIndex());
            int oneBefore = list.get(oneBeforeLastIndex());
            int sum = last + oneBefore;

            list.add(sum);

            return sum;
        }
    };

    private FibonacciCalculator synced = new FibonacciCalculator() {
        @Override
        public void calculate() {
            new Thread(calculateNextElement).start();
            new Thread(calculateNextElement).start();
        }
    };

    private FibonacciCalculator atomic = () -> {
        AtomicInteger last = new AtomicInteger();
        Thread setLast = new Thread(() -> {
            last.set(list.get(lastIndex()));
        });

        AtomicInteger oneBeforeLast = new AtomicInteger();
        Thread setOneBeforeLast = new Thread(() -> {
            oneBeforeLast.set(list.get(oneBeforeLastIndex()));
        });

        list.add(last.addAndGet(oneBeforeLast.get()));
    };

    private FibonacciCalculator singleThreadExecutor = () -> {
        ExecutorService executor = null;

        try {
            executor = Executors.newSingleThreadExecutor();
            executor.execute(calculateNextElement);
            executor.submit(calculateNextElement);
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    };

    private FibonacciCalculator fixedThreadPoolExecutor = () -> {
        ExecutorService executor = null;

        try {
            executor = Executors.newFixedThreadPool(2);

            List<Callable<Integer>> calculators = Arrays.asList(calculateNext,
                    calculateNext);

            executor.invokeAll(calculators).stream().map(future -> {
                try {
                    return future.get();
                } catch (Exception ex) {
                    try {
                        throw ex;
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }).close();
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    };

    private FibonacciCalculator workStealingPoolExecutor = () -> {
        ExecutorService executor = null;

        try {
            executor = Executors.newWorkStealingPool();

            List<Callable<Integer>> calculators = Arrays.asList(calculateNext,
                    calculateNext);

            executor.invokeAny(calculators);
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    };

    private List<Integer> generate(final int size,
                                   FibonacciCalculator fibonacci)
            throws InterruptedException, ExecutionException {
        while (size > list.size()) {
            fibonacci.calculate();
        }

        return list.subList(0, size);
    }

    private int lastIndex() {
        return list.size() - 1;
    }

    private int oneBeforeLastIndex() {
        return list.size() - 2;
    }
}
