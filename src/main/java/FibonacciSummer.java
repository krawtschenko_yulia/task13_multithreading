import java.util.List;
import java.util.concurrent.Callable;

public class FibonacciSummer implements Callable<Integer> {
    private List<Integer> list;
    private int size;

    public FibonacciSummer(List<Integer> list, int size) {
        this.list = list;
        this.size = size;
    }

    public FibonacciSummer(List<Integer> list) {
        new FibonacciSummer(list, list.size());
    }

    @Override
    public Integer call() throws Exception {
        int sum = 0;

        for (int i = 4; i < size; i++) {
            sum += list.get(i - 2) * (size - i);
        }
        sum += 2 * (size - 1);

        return sum;
    }
}
