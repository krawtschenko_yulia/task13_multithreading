public class PingPongRunnable implements Runnable {
    protected enum PingPongWord { PING, PONG };
    private PingPongWord word;
    private Object ball;

    public PingPongRunnable(PingPongWord word, Object ball) {
        this.word = word;
        this.ball = ball;
    }

    @Override
    public void run() {
        synchronized (ball) {
            System.out.println(word);
            try {
                ball.notifyAll();
                ball.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
