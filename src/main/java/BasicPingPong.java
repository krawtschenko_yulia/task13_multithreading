public class BasicPingPong implements PingPongInterface {
    private PingPongRunnable ping;
    private PingPongRunnable pong;

    @Override
    public void throwBall() throws InterruptedException {
        Object ball = new Object();

        ping = new PingPongRunnable(PingPongRunnable.PingPongWord.PING, ball);
        pong = new PingPongRunnable(PingPongRunnable.PingPongWord.PONG, ball);

        Thread pingThread = new Thread(ping);
        Thread pongThread = new Thread(pong);

        pingThread.start();
        Thread.sleep(100);
        pongThread.start();
    }
}
