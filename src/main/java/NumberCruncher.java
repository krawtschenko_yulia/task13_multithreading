import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NumberCruncher {
    final List<Integer> numbers;
    final Object even = new Object();
    final Object odd = new Object();
    final Object relativelyPrime = new Object();

    public NumberCruncher() {
        numbers = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    }

    public List<Integer> removeEvenNumbers() {
        synchronized (numbers) {
            numbers.removeIf(this::isEven);

            return numbers;
        }
    }

    public List<Integer> removeOddNumbers() {
        synchronized (numbers) {
            numbers.removeIf(this::isOdd);

            return numbers;
        }
    }

    public List<Integer> removeRelativelyPrimeNumbers() {
        synchronized (numbers) {
            numbers.removeIf(this::isRelativelyPrime);

            return numbers;
        }
    }

    public List<Integer> getEvenNumbers() {
        synchronized (even) {
            return numbers.stream()
                    .filter(this::isEven)
                    .collect(Collectors.toList());
        }
    }

    public List<Integer> getOddNumbers() {
        synchronized (odd) {
            return numbers.stream()
                    .filter(this::isOdd)
                    .collect(Collectors.toList());
        }
    }

    public List<Integer> getRelativelyPrimeNumbers() {
        synchronized (relativelyPrime) {
            return numbers.stream()
                    .filter(this::isRelativelyPrime)
                    .collect(Collectors.toList());
        }
    }

    private boolean isEven(int number) {
        return number % 2 == 0;
    }

    private boolean isOdd(int number) {
        return number % 2 != 0;
    }

    private boolean isRelativelyPrime(int number) {
        for (Integer n : numbers) {
            if (number != n && n % number == 0) {
                return false;
            }
        }
        return true;
    }
}
