import java.io.IOException;
import java.io.PipedReader;

public class PipedReaderTask implements Runnable {
    private String name;
    private PipedReader reader;

    public PipedReaderTask(String name, PipedReader reader) {
        this.name = name;
        this.reader = reader;
    }

    @Override
    public void run() {
        char ch;
        while (true) {
            try {
                ch = (char) reader.read();
                    System.out.println(ch);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
