import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class BusinessLogic {
    public void launchPingPong() throws InterruptedException {
        BasicPingPong pingPong = new BasicPingPong();
        pingPong.throwBall();
    }

    public void generateFibonacciLists() {
        FibonacciGenerator generator = new FibonacciGenerator();

        Thread generatingShortList = new Thread(() -> {
            try {
                generator.generateList(5);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
        generatingShortList.start();

        Thread generatingLongList = new Thread(() -> {
            try {
                generator.generateList(15);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
        generatingLongList.start();

        Thread generatingMediumLengthList = new Thread(() -> {
            try {
                generator.generateList(10);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
        generatingMediumLengthList.start();
    }

    public void generateFibonacciListsViaExecutors() throws InterruptedException {
        ExecutorService executor = null;

        ThreadGroup group = new ThreadGroup("GeneratingLists");

        try {
            executor = Executors.newSingleThreadExecutor();

            FibonacciGenerator generator = new FibonacciGenerator();

            Thread generatingShortList = new Thread(group, () -> {
                try {
                    generator.generateList(5);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
            executor.submit(generatingShortList);

            Thread generatingLongList = new Thread(group, () -> {
                try {
                    generator.generateList(15);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
            executor.submit(generatingLongList);

            Thread generatingMediumLengthList = new Thread(group, () -> {
                try {
                    generator.generateList(10);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
            executor.submit(generatingMediumLengthList);
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }

        try {
            executor = Executors.newFixedThreadPool(3);

            FibonacciGenerator generator = new FibonacciGenerator();

            List<Callable<List<Integer>>> callableList = new ArrayList<>();

            Callable<List<Integer>> generatingShortList = () -> {
                List<Integer> list = null;
                try {
                    list = generator.generateList(5);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                return list;
            };
            callableList.add(generatingShortList);

            Callable<List<Integer>> generatingLongList = () -> {
                List<Integer> list = null;
                try {
                    list = generator.generateList(15);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                return list;
            };
            callableList.add(generatingLongList);

            Callable<List<Integer>> generatingMediumLengthList = () -> {
                List<Integer> list = null;
                try {
                    list = generator.generateList(10);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                return list;
            };
            callableList.add(generatingMediumLengthList);

            executor.invokeAll(callableList);
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    }

    public void calculateFibonacciSums() throws ExecutionException,
            InterruptedException {
        FibonacciGenerator generator = new FibonacciGenerator();

        List<Integer> fibonacciShort = generator.generateList(5);
        List<Integer> fibonacciLong = generator.generateList(15);
        List<Integer> fibonacciMedium = generator.generateList(10);

        Callable<Integer> sumShort = new FibonacciSummer(fibonacciShort);
        Callable<Integer> sumLong = new FibonacciSummer(fibonacciLong);
        Callable<Integer> sumMedium = new FibonacciSummer(fibonacciMedium);

        ExecutorService executor = null;

        try {
            executor = Executors.newFixedThreadPool(3);

            executor.invokeAll(Arrays.asList(
                    sumShort, sumLong, sumMedium
            ));
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    }

    public void managePoolOfSleepers() {
        ScheduledExecutorService executor = null;

        try {
            executor = Executors.newScheduledThreadPool(3);

            for (int i = 0; i < 5; i++) {
                executor.schedule(new Sleeper(1, 10),
                        i, TimeUnit.MILLISECONDS);
            }
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    }

    public void launchConflictingMethods() throws InterruptedException {
        NumberCruncher numberCruncher = new NumberCruncher();
        List<Callable<List<Integer>>> dependent = new ArrayList<>();

        Callable<List<Integer>> removeEven = numberCruncher::removeEvenNumbers;
        dependent.add(removeEven);

        Callable<List<Integer>> removeOdd = numberCruncher::removeOddNumbers;
        dependent.add(removeOdd);

        Callable<List<Integer>> removeRelativelyPrime =
                numberCruncher::removeRelativelyPrimeNumbers;
        dependent.add(removeRelativelyPrime);

        ExecutorService executor = null;
        try {
            executor = Executors.newFixedThreadPool(3);
            executor.invokeAll(dependent);
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    }

    public void launchNonConflictingMethods() throws InterruptedException {
        NumberCruncher numberCruncher = new NumberCruncher();
        List<Callable<List<Integer>>> independent = new ArrayList<>();

        Callable<List<Integer>> getEven = numberCruncher::getEvenNumbers;
        independent.add(getEven);

        Callable<List<Integer>> getOdd = numberCruncher::getOddNumbers;
        independent.add(getOdd);

        Callable<List<Integer>> getRelativelyPrime
                = numberCruncher::getRelativelyPrimeNumbers;
        independent.add(getRelativelyPrime);

        ExecutorService executor = null;
        try {
            executor = Executors.newFixedThreadPool(3);
            executor.invokeAll(independent);
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    }

    public void launchMethodsWithLocks() {
        NumberCruncherWithLocks numberCruncher =
                new NumberCruncherWithLocks();

        Callable<List<Integer>> getOdd = numberCruncher::getOddNumbers;
        Callable<List<Integer>> removeRelativelyPrime =
                numberCruncher::removeRelativelyPrimeNumbers;

        ExecutorService executor = null;
        try {
            executor = Executors.newFixedThreadPool(2);
            executor.submit(getOdd);
            executor.submit(removeRelativelyPrime);
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    }

    public void usePipe() throws IOException {
        PipedReader reader = new PipedReader();
        PipedWriter writer = new PipedWriter();
        writer.connect(reader);

        Thread reading = new Thread(
                new PipedReaderTask("Reading", reader)
        );
        Thread writing = new Thread(
                new PipedWriterTask("Writing", writer)
        );
        reading.start();
        writing.start();
    }

    public void useBlockingQueue() {
        BlockingQueue<Integer> q = new ArrayBlockingQueue<Integer>(512);

        QueuingReaderTask read = new QueuingReaderTask("Reading", q);
        QueuingWriterTask write = new QueuingWriterTask("Writing", q);

        ExecutorService executor = null;
        try {
            executor = Executors.newFixedThreadPool(2);
            executor.submit(read);
            executor.submit(write);
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }
    }
}
