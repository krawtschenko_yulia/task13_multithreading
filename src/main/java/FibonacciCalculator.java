import java.util.concurrent.ExecutionException;

public interface FibonacciCalculator {
    void calculate() throws InterruptedException, ExecutionException;
}
